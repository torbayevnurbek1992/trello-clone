<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['title', 'order_num', 'column_id'];

    public static function getByColumn(int $columnId) {
        return static::where(['column_id' => $columnId])->orderBy('order_num', 'asc')->get();
    }
}
