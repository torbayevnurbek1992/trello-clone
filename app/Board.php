<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = ['title', 'type'];

    public static function getOpened() {
        return static::where(['status' => 'open'])->with('columns')->orderBy('id', 'desc')->get();
    }

    public function columns()
    {
        return $this->hasMany(Column::class)
            ->with('cards')->orderBy('order_num', 'asc');
    }
}
