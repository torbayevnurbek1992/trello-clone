<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    protected $fillable = ['title', 'order_num', 'board_id'];

    public static function getByBoard(int $boardId) {
        return static::where(['board_id' => $boardId])->with('cards')->orderBy('order_num', 'asc')->get();
    }

    public function cards() {
        return $this->hasMany(Card::class)->orderBy('order_num', 'asc');
    }
}
