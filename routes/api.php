<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('boards', 'BoardController@index');
Route::post('boards', 'BoardController@store');
Route::get('boards/{board}', 'BoardController@show');

Route::get('columns/board/{board_id}', 'ColumnController@board');
Route::post('columns', 'ColumnController@store');

Route::get('cards/column/{column_id}', 'CardController@column');
Route::post('cards', 'CardController@store');
