import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class Nav extends Component {
    render() {
        return (
            <div id="main-navbar" className="bg-dark-transparent d-flex flex-md-row flex-column align-items-center
            p-1 px-md-3 text-white">
                <h5 className="my-0 mr-md-auto font-weight-normal">
                    <Link to="/" className="text-white">ТуДу</Link>
                </h5>
                <nav className="my-2 my-md-0 mr-md-3">
                    <a className="p-2 text-white" href="#">О проекте</a>
                </nav>
                <a className="btn btn-outline-light" href="#">Войти</a>
            </div>
        );
    }
}