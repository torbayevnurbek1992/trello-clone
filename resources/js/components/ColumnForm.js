import React, {Component} from 'react';
import axios from 'axios';

export default class ColumnForm extends Component {
    constructor (props) {
        super(props);
        this.state = {
            title: '',
            order_num: 1,
            board_id: props.board_id,
            errors: {}
        };
    }

    //отслеживаем изменения обычных инпутов
    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    createNewColumn(e) {
        e.preventDefault();

        let $boardBody = e.target.closest('#board-body');
        let $listItems = $boardBody.querySelectorAll('.list-item');
        let orderNum = $listItems.length + 1;

        const card = {
            title: this.state.title,
            order_num: orderNum,
            board_id: this.state.board_id
        };

        axios.post('/api/columns', card)
            .then(response => {
                this.props.fetchToggle();
            })
            .catch(error => {
                if (error.response && error.response.data.errors) {
                    this.setState({
                        errors: error.response.data.errors // заполняем объект с ошибками, если они есть.
                    });
                }
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field];
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                  <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    }

    render () {
        return (
            <form className="bg-custom-light p-2 shadow-sm border-radius" onSubmit={this.createNewColumn.bind(this)}>
                <div className="form-group mb-2">
                    <input
                        className={`form-control shadow-sm ${this.hasErrorFor('title') ? 'is-invalid' : ''}`}
                        name="title"
                        placeholder="Название списка"
                        onChange={this.handleFieldChange.bind(this)}
                    />
                    {this.renderErrorFor('title')}
                </div>
                <button type="submit" className="btn btn-primary">Добавить список</button>
                <button type="button" onClick={this.props.onCancel} className="ml-2 btn btn-outline-secondary">
                    <i className="fa fa-times">
                    </i>
                </button>
            </form>
        );
    }
};