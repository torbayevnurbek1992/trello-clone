import React, { Component } from 'react';
import axios from 'axios';

export default class BoardCreate extends Component {
    constructor(props) {
        super(props); //props хранит в себе объект history
        console.log(props);
        this.state = {
            title: '',
            type: 'public',
            errors: {}
        };

        //делается для того чтобы вызов внутри функции render был короче
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleCreateNewBoard = this.handleCreateNewBoard.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
    }

    //отслеживаем изменения обычных инпутов
    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    //отслеживаем изменения radio инпутов. Разнца в currentTarget и target
    handleRadioChange(event) {
        this.setState({
            [event.currentTarget.name]: event.currentTarget.value
        });
    }

    //Нажатие на отправить
    handleCreateNewBoard (event) {
        event.preventDefault();

        const { history } = this.props;

        const board = {
            title: this.state.title,
            type: this.state.type
        };

        axios.post('/api/boards', board)
            .then(response => {
                history.push('/'); // redirect
            })
            .catch(error => {
                if (error.response && error.response.data.errors) {
                    this.setState({
                        errors: error.response.data.errors // заполняем объект с ошибками, если они есть.
                    });
                }
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field];
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                  <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    }

    render () {
        return (
            <div className='container'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header'>Создание новой доски</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateNewBoard}>
                                    <div className='form-group'>
                                        <label htmlFor='title'>Название</label>
                                        <input
                                            id='title'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('title') ? 'is-invalid' : ''}`}
                                            name='title'
                                            value={this.state.title}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('title')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='type'>Тип доски</label>
                                        <div className="custom-control custom-radio">
                                            <input
                                                type="radio"
                                                id="customRadio1"
                                                name="type"
                                                className="custom-control-input"
                                                value="private"
                                                checked={this.state.type === 'private'}
                                                onChange={this.handleRadioChange} />
                                                <label className="custom-control-label" htmlFor="customRadio1">
                                                    Приватная</label>
                                        </div>
                                        <div className="custom-control custom-radio">
                                            <input type="radio"
                                                   id="customRadio2"
                                                   name="type"
                                                   value="public"
                                                   className="custom-control-input"
                                                   checked={this.state.type === 'public'}
                                                   onChange={this.handleRadioChange} />
                                                <label className="custom-control-label" htmlFor="customRadio2">
                                                    Публичная</label>
                                        </div>
                                        {this.renderErrorFor('type')}
                                    </div>
                                    <button className='btn btn-primary'>Создать</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}