import React from 'react';

const ColumnAddBtn = props => {
    if (props && props.onClick) {
        return (
            <button
                type="button"
                className="bg-dark-transparent pl-3 pr-3 pt-2 pb-2 btn btn-sm btn-block btn-add-column text-light"
                onClick={props.onClick}>
                Добавить список
            </button>
        );
    }
};

export default ColumnAddBtn;