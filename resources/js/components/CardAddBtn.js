import React from 'react';

const CardAddBtn = props => {
    if (props && props.onClick) {
        return (
            <button
                type="button"
                className="pl-3 pr-3 pt-2 pb-2 btn btn-sm btn-block btn-add-card"
                onClick={props.onClick}>
                Добавить карточку
            </button>
        );
    }
};

export default CardAddBtn;