import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';

export default class Boards extends Component {

    constructor() {
        super();
        this.state = {
            boards: []
        }
    }

    componentDidMount() {
        axios.get('/api/boards').then(response => {
            this.setState ({
                boards: response.data
            });
        }).catch(error => {
            console.log(error.response);
        });
    }

    render() {
        const { boards } = this.state;
        return (

            <div id="boards" className="container-fluid pt-2">
                <div className="card-columns">
                    <Link to="/boards/create" className="btn card bg-primary text-white text-left">
                        <div className="card-body">
                            <h5 className="card-title">
                                Добавить доску
                            </h5>
                        </div>
                    </Link>

                    {boards.map(board => (
                        <Link to={'/boards/' + board.id} key={board.id} className="card btn bg-light text-left">
                            <div className="card-body">
                                <h5 className="card-title">{board.title}</h5>
                                <p className="card-text">
                                    <small className="text-muted">{board.created_at}</small>
                                </p>
                            </div>
                        </Link>
                    ))}
                </div>
            </div>
        );
    }
}