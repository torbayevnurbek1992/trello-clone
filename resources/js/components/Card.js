import React, {Component} from 'react';

export default class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            card: props.card,
        };
    }

    render() {
        return (
            <div className="card-item mt-2 shadow-sm bg-white border-radius p-2">
                <div className="card-item-body">
                    <span>{this.state.card.title}</span>
                </div>
                <div className="card-item-footer">
                </div>
            </div>
        );
    }
}