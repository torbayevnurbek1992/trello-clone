import React, {Component} from 'react';
import axios from 'axios';

export default class CardForm extends Component {
    constructor (props) {
        super(props);
        this.state = {
            title: '',
            order_num: 1,
            column_id: props.column_id,
            errors: {}
        };
    }

    //отслеживаем изменения обычных инпутов
    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    createNewCard(e) {
        e.preventDefault();

        let $listItem = e.target.closest('.list-item');
        let $cardItems = $listItem.querySelectorAll('.card-item');
        let orderNum = $cardItems.length + 1;

        const card = {
            title: this.state.title,
            order_num: orderNum,
            column_id: this.state.column_id
        };

        axios.post('/api/cards', card)
            .then(response => {
                this.props.fetchToggle();
            })
            .catch(error => {
                if (error.response && error.response.data.errors) {
                    this.setState({
                        errors: error.response.data.errors // заполняем объект с ошибками, если они есть.
                    });
                }
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field];
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                  <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    }

    render () {
        return (
            <form className="pl-2 pr-2 pb-2" onSubmit={this.createNewCard.bind(this)}>
                <div className="form-group mb-2">
                    <textarea
                        className={`form-control shadow-sm ${this.hasErrorFor('title') ? 'is-invalid' : ''}`}
                        name="title"
                        placeholder="Название карточки"
                        onChange={this.handleFieldChange.bind(this)}>
                    </textarea>
                    {this.renderErrorFor('title')}
                </div>
                <button type="submit" className="btn btn-primary">Добавить карточку</button>
                <button type="button" onClick={this.props.onCancel} className="ml-2 btn btn-outline-secondary">
                    <i className="fa fa-times">
                    </i>
                </button>
            </form>
        );
    }
};