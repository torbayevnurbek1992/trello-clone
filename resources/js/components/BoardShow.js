import React, { Component } from 'react';
import axios from 'axios';
import Column from './Column';
import ColumnForm from './ColumnForm';
import ColumnAddBtn from './ColumnAddBtn';

export default class BoardShow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            board: {},
            showForm: false,
            fetchToggle: true
        };

        this.fetchColumns = this.fetchColumns.bind(this);
        this.fetchToggle = this.fetchToggle.bind(this);
        this.columnFormToggle = this.columnFormToggle.bind(this);
    }

    columnFormToggle() {
        const { board } = this.state;
        board.showForm === true ? board.showForm = false : board.showForm = true;

        this.setState(board);
    }

    fetchToggle() {
        this.setState({
            fetchToggle: !this.state.fetchToggle
        });
    }

    componentDidMount() {
        const boardId = this.props.match.params.id;
        axios.get('/api/boards/' + boardId).then(response => {
            this.setState ({
                board: response.data
            });

            if (this.state.board.columns) {
                this.state.board.columns.forEach(function (item) {
                    item.showForm = false;
                });
            }
        }).catch(error => {
            console.log(error.response);
        });
    }

    fetchColumns() {
        const boardId = this.props.match.params.id;

        axios.get('/api/columns/board/' + boardId).then(response => {
            const board = this.state.board;
            board.columns = response.data;

            this.setState ({
                board: board
            });

            if (this.state.board.columns) {
                this.state.board.columns.forEach(function (item) {
                    item.showForm = false;
                });
            }
        }).catch(error => {
            console.log(error.response);
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.fetchToggle !== this.state.fetchToggle){
            this.fetchColumns();
            this.columnFormToggle();
        }
    }

    render() {
        let columnLastItem;
        if (this.state.showForm === true) {
            columnLastItem = <ColumnForm
                board_id={this.props.match.params.id}
                onCancel={this.columnFormToggle}
                fetchToggle={this.fetchToggle}/>;
        } else {
            columnLastItem = <ColumnAddBtn onClick={this.columnFormToggle}/>;
        }

        return (
            <div id="board">
                <div className="container-fluid">
                    <div id="board-header" className="bg-light-dark-transparent text-white row align-items-center">
                        <div className="col-auto">
                            <h1>{this.state.board.title}</h1>
                        </div>
                        <div className="col-auto">
                            <span>{this.state.board.type}</span>
                        </div>
                    </div>

                    {this.state.board.columns &&
                        <div id="board-body" className="mt-2">
                            <div className="d-flex flex-nowrap align-items-start">
                                {this.state.board.columns.map(column => (
                                    <Column
                                        column={column}
                                        key={column.order_num}
                                        fetchToggle={this.fetchToggle}
                                    />
                                ))}
                                <div className="list-item-wrapper border-radius ml-1 mr-1">
                                    {columnLastItem}
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </div>
        );
    }
}