import React, {Component} from 'react';

import CardForm from './CardForm';
import CardAddBtn from './CardAddBtn';
import Card from './Card';
import axios from "axios/index";

export default class Column extends Component {
    constructor(props) {
        super(props);
        this.state = {
            column: props.column,
            fetchToggle: true
        };

        this.fetchCards = this.fetchCards.bind(this);
        this.fetchToggle = this.fetchToggle.bind(this);
        this.cardFormToggle = this.cardFormToggle.bind(this);
    }

    cardFormToggle() {
        const { column } = this.state;
        column.showForm === true ? column.showForm = false : column.showForm = true;

        this.setState(column);
    }

    fetchToggle() {
        this.setState({
            fetchToggle: !this.state.fetchToggle
        });
    }

    fetchCards() {
        axios.get('/api/cards/column/' + this.state.column.id).then(response => {
            const column = this.state.column;
            column.cards = response.data;

            this.setState ({
                column: column
            });

        }).catch(error => {
            console.log(error.response);
        });
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.fetchToggle !== this.state.fetchToggle){
            this.fetchCards();
            this.cardFormToggle();
        }
    }

    render () {
        let listItemFooter;
        if (this.state.column.showForm === true) {
            listItemFooter = <CardForm
                column_id={this.state.column.id}
                onCancel={this.cardFormToggle}
                fetchToggle={this.fetchToggle}/>;
        } else {
            listItemFooter = <CardAddBtn onClick={this.cardFormToggle}/>;
        }

        return (
            <div className="list-item-wrapper bg-custom-light list-item border-radius ml-1 mr-1 shadow-sm">
                <div className="list-item-header pl-3 pr-3 pt-3 pb-2">
                    <h6 className="m-0">{this.state.column.title}</h6>
                </div>
                {this.state.column.cards &&
                <div className="list-item-body pl-2 pr-2 pb-2">
                    {this.state.column.cards.map(card => (
                        <Card card={card} key={card.order_num} />
                    ))}
                </div>
                }
                <div className="list-item-footer">
                    {listItemFooter}
                </div>
            </div>
        )
    }
};