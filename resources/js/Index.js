import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'font-awesome/css/font-awesome.min.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import Nav from './components/Nav';
import Boards from './components/Boards';
import BoardCreate from './components/BoardCreate';
import BoardShow from './components/BoardShow';


export default class Index extends Component {
    render() {
        return (
            <div id="react-main-block">
                <Router>
                    <Nav />
                    <Switch>
                        <Route path='/' exact component={Boards} />
                        <Route path='/boards' exact component={Boards} />
                        <Route path='/boards/create' exact component={BoardCreate} />
                        <Route path='/boards/:id' exact component={BoardShow} />
                    </Switch>
                </Router>
            </div>
        );
    }
}

if (document.getElementById('main-block')) {
    ReactDOM.render(<Index />, document.getElementById('main-block'));
}